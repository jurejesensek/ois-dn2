function divElementEnostavniTekst(sporocilo) {
  //return $('<div style="font-weight: bold"></div>').text(sporocilo);
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var zacetnoSporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  

  var chars = zacetnoSporocilo.split("");   // razbije na posamezne znake
  var sporocilo = "";
  
  for (var i = 0; i < chars.length; i++) {    // zamenja znak po znak (< in >)
    chars[i] = chars[i].replace("<", "&lt;");
    chars[i] = chars[i].replace(">", "&gt;");
    sporocilo += chars[i];
  }
  
  for (var i = 0; i < filterBesed.length; i++) {
    var regex = "\\b" + filterBesed[i] + "\\b";
    sporocilo = sporocilo.replace(new RegExp(regex, "gi"), function() {
      var zvezdice = "";
      for (var j = 0; j < filterBesed[i].length; j++) {
        zvezdice += "*";
      }
      return zvezdice;
    });
  }

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var arraySporocilo = sporocilo.split(" ");    // razdeli na besede
    var rezultat = "";
    for(var i = 0; i < arraySporocilo.length; i++) {    // se sprehodi cez vse besede
      arraySporocilo[i] = arraySporocilo[i].replace(":)", "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">");
      arraySporocilo[i] = arraySporocilo[i].replace(";)", "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">");
      arraySporocilo[i] = arraySporocilo[i].replace(":*", "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">");
      arraySporocilo[i] = arraySporocilo[i].replace(":(", "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">");
      arraySporocilo[i] = arraySporocilo[i].replace("(y)", "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">");
      // naredi zamenjave
      
      rezultat += (arraySporocilo[i] + " ");    // doda k sporocilu
    }
    
    var kanal = $('#kanal').text();
    var afna = kanal.indexOf("@");
    var tekst = kanal.substring(afna + 2, kanal.length);
    klepetApp.posljiSporocilo(tekst, rezultat);
    $('#sporocila').append(divElementEnostavniTekst(rezultat));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      var upImeKanal = $('#kanal').text();
      var afna = upImeKanal.indexOf("@");
      var kanal = upImeKanal.substring(afna +2, upImeKanal.length);
      $('#kanal').text(rezultat.vzdevek + " @ " + kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) { 
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    //var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('zasebnoSporocilo', function(zasebnoSporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(zasebnoSporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  socket.on('napakaZasebno', function(info) {
      $('#sporocila').append(divElementHtmlTekst("Sporočila " + info.besedilo + " uporabniku z vzdevkom " + info.prejemnik + " ni bilo mogoče posredovati."));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
// komentar za commit
